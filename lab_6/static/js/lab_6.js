//chat
var pesan = document.getElementById('msgToSubmit');
var submitPesan = document.getElementById('submittedMsg');
var countChat = 0;

if (localStorage.historyChat){
	submitPesan.innerHTML = localStorage.historyChat;
} else{
	localStorage.historyChat="";
}

function sendMassage(event) {
  var keyPressed  = event.keycode || event.which;
  if (keyPressed==13){
  	if((pesan.innerHTML != " ") || pesan.innerHTML != null) {
     if (countChat % 2 == 0){
      var snd = "msg-send";
  		var sender = "ester :";
  		var kirimPesan = sender + pesan.value;
  		localStorage.historyChat=localStorage.historyChat+'<p class="'+snd+'">'+kirimPesan+'</p>';
  		submitPesan.innerHTML=localStorage.historyChat;
  		pesan.value=null;
  		countChat+=1;'<p class="msg-send">kirimPesan</p>'
  	}
  	else{
  		var snd = "msg-receive";
      var receiver = "santo :";
  		var kirimPesan = receiver + pesan.value;
  		localStorage.historyChat=localStorage.historyChat+'<p class="'+snd+'">'+kirimPesan+'</p>';
  		submitPesan.innerHTML=localStorage.historyChat;
  		pesan.value=null;
  		countChat+=1;
  	}
  }
}
}

  function hideChat() {
  var chatBox = document.getElementById('chat-body');
    if(chatBox.style.display === "block") {
      chatBox.style.display = "none";
      document.getElementById('arrowIcon').style.transform = "rotateZ(180deg)";
      document.getElementById('arrowIcon').title = "Show Chat";
    } else {
      chatBox.style.display = "block"
      document.getElementById('arrowIcon').style.transform = "rotateZ(0deg)";
      document.getElementById('arrowIcon').title = "Hide Chat";
    }
  }
}

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
  	print.value="";
  	erase = false;
    /* implemetnasi clear all */
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (x=='sin'){
  	print.value = Math.round(evil(Math.sin(print.value))*10000)/10000;
  	erase=true;
  }
  else if (x==='log'){
  	print.value=Math.log(print.value);
  	erase=true;
  }
  else if (x==='tan'){
  	print.value= Math.tan(print.value);
  	erase=true;
  }
  else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}


//Change themes
if (typeof Storage !== "undefined"){
var themesOri = [
	    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
	    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
	    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
	    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
	    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
	    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
	    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
	    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
	    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
	    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
	    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];
localStorage.themesOri=JSON.stringify(themesOri);
}
else{
	window.alert("Sorry, your themes can't be changed")
}
var defaultTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};

$(document).ready(function() {
	$('.my-select').select2({
		'data':JSON.parse(localStorage.themesOri),
	})

	if(localStorage.defaultTheme){
		var saveTheme = JSON.parse(localStorage.defaultTheme);
		$('body').css({"background-color": saveTheme.Indigo.bcgColor,"color":saveTheme.Indigo.fontColor});
	}else{
		localStorage.defaultTheme=JSON.stringify(defaultTheme);
		$('body').css({"background-color": defaultTheme.Indigo.bcgColor, "color": defaultTheme.Indigo.fontColor});
	}

    $('.apply-button').on('click', function() {
    	var theme_value = $('.my-select').val();
    	var selected_Theme = themesOri[theme_value];
    	$('body').css({"background-color" : selected_Theme.bcgColor, "color" : selected_Theme.fontColor});
    	var text_color = selected_Theme.text;
    	var background_color = selected_Theme.bcgColor;
    	var font_color = selected_Theme.fontColor;
    	var test = {"Indigo":{"bcgColor":background_color,"fontColor":font_color}};
    	localStorage.defaultTheme = JSON.stringify(test);
    })
});


// if(localStorage.getItem('themes')==null){
// 	localStorage.setItem('themes', JSON.stringify(themesOri));
// }
// else if(localStorage.getItem('selectedTheme')== null){
// 	localStorage.setItem('selectedTheme', JSON.stringify(defaultTheme));
// }
// var getThemes = JSON.parse(localStorage.getItem('themes'));
// var getSelectedTheme = JSON.parse(localStorage.getItem('selectedTheme'));
// changeTheme(getThemes[3]);

// $(document).ready(function() {
//    $('.my-select').select2({'data': getThemes}).val(getSelectedTheme['id']);
//    $('.apply-button').on('click', function(){
//    		getSelectedTheme=getThemes[$('.my-select').val()];
//    		changeTheme(getSelectedTheme);
//    		localStorage.setItem('selectedTheme', JSON.stringify(getSelectedTheme));
//    })
// });

// function changeTheme(color){
// 	$('body').css({
// 		"backgroundColor": color.bcgColor
// 	});
// 	$('.text-center').css({
// 		"color" : color.fontColor
// 	});
// }