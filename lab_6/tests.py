from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

class Lab6UniTest(TestCase):
	def test_lab6_url_is_exist(self):
		response = Client().get('/lab-6/')
		self.assertEqual(response.status_code, 200)
  
	def test_lab6_using_indexFunction(self):
		found=resolve('/lab-6/')
		self.assertEqual(found.func, index)

# Create your tests here.

