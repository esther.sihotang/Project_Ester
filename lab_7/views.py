from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Friend, Mahasiswa
from .api_csui_helper.csui_helper import CSUIhelper
from django.core import serializers
import os
import json
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

response = {}
csui_helper = CSUIhelper()

def index(request):
        mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
        for data in mahasiswa_list:
            Mahasiswa.objects.get_or_create(nama=data['nama'], npm=data['npm'])
        friend_list = Friend.objects.all()
        mahasiswa_list=Mahasiswa.objects.all().order_by('pk')
        page=request.GET.get('page',1)
        paginate_data=paginate_page(page, mahasiswa_list)
        mhs = paginate_data['data']
        page_range = paginate_data['page_range']

        html='lab_7/lab_7.html'
        response={"mahasiswa_list" : mhs,"friend_list": friend_list, "page_range":page_range}
        return render(request,html,response)

def paginate_page(page, data_list):
    paginator= Paginator(data_list, 10)
    data = paginator.page(page)
    # get index of current page
    index=data.number-1
    max_index = len (paginator.page_range)
    # Value is max  index of ur pages, so the last page-1
    data=paginator.page(page)
    # We want to range of 1-,so let calculate where to slice the friend_list
    
    if index>=10:
        start_index=index
    else:
        start_index=0

    if index<max_index-5:
        end_index=10
    # start_index = index if index >=10 else 0
    # end_index=5 if index<max_index-5 else max_index

    page_range = list(paginator.page_range)[start_index:end_index]
    paginate_data={'data':data, 'page_range':page_range}
    return paginate_data

def friend_list(request):
        friend_list = Friend.objects.all()
        page = request.GET.get('page', 1)
        pagination_data=paginate_page(page,friend_list)
        mhs=pagination_data['data']
        page_range = pagination_data['page_range']

        response["friend_list":mhs,"page_range":page_range] 
        html='lab_7/daftar_teman.html'
        return render(request,html,response)

def friend_list_json(request): #update
        friends = Friend.objects.all()
        return HttpResponse(serializers.serialize("json", friends))

@csrf_exempt
def add_friend(request):
        if request.method == 'POST':
                npm = request.POST['npm']
                name = request.POST['name']
                nama1= Friend.objects.filter(friend_name=name).count()
                npm1=Friend.objects.filter(npm=npm).count()

                friend = Friend(friend_name=name, npm=npm)
                friend.save()
                data=model_to_dict(friend)
                return HttpResponse(data)

def delete_friend(request, friend_id):
    Friend.objects.filter(id-friend_id).delete()
    return HttpResponseRedirect('/lab-7/')

@csrf_exempt

def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm=npm).exists()
    }
    return JsonResponse(data)
        
def model_to_dict(obj):
    data=serializers.serialize('json',[obj,])
    struct=json.loads(data)
    data=json.dumps(struct[0]["fields"])
    return data

def check_friend(coba):
    return Friend.objects.filter(npm=coba).count()>0

def tambah_teman(request):
    if(request.method == "POST"):
        npm=request.POST['npm']
        mhsw=Mahasiswa.objects.get(npm=npm)
        friend=Friend(friend_name=mhsw.name, npm=npm)
        friend.save()
        return HttpResponseRedirect('/lab-7/')