from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Friend, Mahasiswa
from .views import index, add_friend, validate_npm, delete_friend, friend_list_json, paginate_page, check_friend, tambah_teman
from .api_csui_helper.csui_helper import CSUIhelper
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseNotFound, HttpRequest

# Create your tests here.
class Lab7UnitTest(TestCase):
	def setUp(self):
		model = Friend
		self.m = model(friend_name="www", npm="1506797242")
		self.m.save()
		self.cs = CSUIhelper()
		model2 = Mahasiswa 
		self.m2=model2(nama="HUGO REYNALDO", npm="0606101452")
		self.m2.save()

	# def test_lab7_url_is_exist(self):
	# 	response = Client().get('/lab-7/')
	# 	self.assertEqual(response.status_code, 200)

	# def test_lab7_using_index_function():
	# 	found = resolve('lab-7')
	# 	self.assertEqual(found.func, index)

	# def test_lab_7_url_exist():
	# 	response=Client().get('/lab-7/get-friend-list/')
	# 	self.assertEqual(response.status_code, 200)

	def test_lab7_pagination(self):
		response=Client().get('/lab-7/')
		html_response=response.content.decode('utf8')
		self.assertIn("HUGO REYNALDO (0606101452)",html_response)

	def test_lab_7_validate_npm(self):
		npm='1506797242'
		req=Client().post('/lab-7/validate-npm/',{
			'npm':npm
		})
		self.assertEqual(type(req), JsonResponse)

	# def test_delete_friend(self):
	# 	nama = 'HUGO REYNALDO'
	# 	npm = '0606101452'
	# 	response_post= Client().post('/lab-7/add-friend', {'name': nama, 'npm': npm}) #post(link metodnya, isi buat si metod)
	# 	self.assertEqual(response_post.status_code,301)

	# 	response=Client().get('/lab-7/')
	# 	html_response=response.content.decode('utf8')
	# 	self.assertIn(nama,html_response)

	# 	response=Client().post('/lab-7/friend-list/delete-friend/1/')
	# 	self.assertEqual(response.status_code,302)

	# 	response=Client().get('/lab-7/friend-list')
	# 	html_response=response.content.decode('utf8')
	# 	self.assertNotIn(nama,html_response)

	def test_add_friend(self):
		request=HttpRequest()
		nama ='HUGO REYNALDO'
		npm ='0606101452'
		request.method='POST'
		request.POST={'name':nama,'npm':npm}
		response=add_friend(request)
		countFriend = Friend.objects.all().count()
		self.assertEqual(countFriend,2)

	def test_lab7_models_friend(self):
		str_test="0606101452 - HUGO REYNALDO"
		self.assertEqual(Mahasiswa.objects.all().count(),1)
		self.assertEqual(str_test,self.m2.__str__())

	# def test_csui_helper_get_mahasiswa():
	# 	test_csui_helper = CSUIhelper(username='tes', password='12345')
	# 	mhs_list = test_csui_helper.get_mahasiswa_list()
	# 	self.assertEqual(len(test_csui_helper.get_mahasiswa_list()),0)

	# def  test_csui_helper_get_client_ID(self):
	# 	test_csui_helper=CSUIhelper()
	# 	mhs_list=test_csui_helper.get_client_id()
	# 	self.assertEqual(mhs_list,'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')

	# def test_csui_helper_get_auth_param_dict(self):
	# 	test_csui_helper=CSUIhelper()
	# 	mhs_list=test_csui_helper.get_auth_param_dict()
	# 	self.assertEqual(mhs_list['client_id'],'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')
	def test_lab_7_auth_param_dict(self):
		csui_helper = CSUIhelper()
		auth_param = csui_helper.instance.get_auth_param_dict()
		self.assertEqual(auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])