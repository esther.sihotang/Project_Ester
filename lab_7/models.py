from __future__ import unicode_literals
from django.db import models
# Create your models here.
class Friend(models.Model):
        friend_name = models.CharField(max_length=400)
        npm = models.CharField(max_length=250, unique=True)
        added_at = models.DateField(auto_now_add=True)

        def as_dict(self):
                return {
                        "friend_name": self.friend_name,
			"npm": self.npm,
		}
        
        def __str__(self):
                return self.npm +" - "+self.friend_name,
	

class Mahasiswa(models.Model):
	nama = models.CharField(max_length=30)
	npm=models.CharField(max_length=20, unique=True)

	def __str__(self):
		return self.npm+" - "+self.nama
