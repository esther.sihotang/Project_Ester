//chat
var pesan = document.getElementById('MsgToSend');
var submitPesan = document.getElementById('MsgToSubmit');

$('.chat-head').click(function(){
	$('.chat-body').toggle();
    
});

if (localStorage.chat){
	submitPesan.innerHTML = localStorage.chat;
} else{
	localStorage.chat="";
}



function SendMassage(e) {
  var countChat = 0;
  var code = e.keycode || e.which;
  if (code=13){
  	if((pesan.innerHTML != " ") && countChat % 2 == 0){
  		var sender = "ester :";
  		var snd = "msg-send";
  		var kirimPesan = sender + pesan.value;
  		localStorage.chat=localStorage.chat+'<p class="'+cls+'">'+userMessage+'</p>';
  		submitPesan.innerHTML=localStorage.chat;
  		pesan.value=null;
  		countChat+=1;
  	}
  	else{
  		var sender = "santo :";
  		var snd = "msg-receiver";
  		var kirimPesan = sender + pesan.value;
  		localStorage.chat=localStorage.chat+'<p class="'+cls+'">'+userMessage+'</p>';
  		submitPesan.innerHTML=localStorage.chat;
  		pesan.value=null;
  		countChat+=1;
  	}
  }

}


// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
  	print.value="";
  	erase = false;
    /* implemetnasi clear all */
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (x=='sin'){
  	print.value = Math.round(evil(Math.sin(print.value))*10000)/10000;
  	erase=true;
  }
  else if (x==='log'){
  	print.value=Math.log(print.value);
  	erase=true;
  }
  else if (x==='tan'){
  	print.value= Math.tan(print.value);
  	erase=true;
  }
  else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}


//Change themes

var themesOri = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];
var defaultTheme = {"Orange":{"bcgColor":"#FF5722","fontColor":"#212121"}};
if(localStorage.getItem('themes')==null){
	localStorage.setItem('themes', JSON.stringify(themesOri));
}
if(localStorage.getItem('selectedTheme')== null){
	localStorage.setItem('selectedTheme', JSON.stringify(defaultTheme));
}
var getThemes = JSON.parse(localStorage.getItem('themes'));
var getSelectedTheme = JSON.parse(localStorage.getItem('selectedTheme'));
changeTheme(getThemes[3]);

$(document).ready(function() {
   $('.my-select').select2({'data': getThemes}).val(getSelectedTheme['id']);
   $('.apply-button').on('click', function(){
   		getSelectedTheme=getThemes[$('.my-select').val()];
   		changeTheme(getSelectedTheme);
   		localStorage.setItem('selectedTheme', JSON.stringify(getSelectedTheme));
   })
});

function changeTheme(color){
	$('body').css({
		"backgroundColor": color.bcgColor
	});
	$('.text-center').css({
		"color" : color.fontColor
	});
}