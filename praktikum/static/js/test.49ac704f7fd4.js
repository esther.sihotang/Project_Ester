    $( document ).ready(function() {
    var button_8 = $('button:contains("8")');
    var button_4 = $('button:contains("4")');

    var button_add = $('button:contains("+")');
    var button_sub = $('button:contains("-")');
    var button_mul = $('button:contains("*")');
    var button_div = $('button:contains("/")');

    var button_clear = $('button:contains("AC")');
    var button_res = $('button:contains("=")');

    QUnit.test( "Addition Test", function( assert ) {
      button_8.click();
      button_add.click();
      button_4.click();
      button_res.click();
      assert.equal( $('#print').val(), 12, "8 + 4 must be 12" );
      button_clear.click();
    });

      QUnit.test( "Substraction Test", function( assert ) {
      button_8.click();
      button_sub.click();
      button_1.click();
      button_res.click();
      assert.equal( $('#print').val(), 7, "8 - 1 must be 7" );
      button_clear.click();
    });

      QUnit.test("Division Test", function(assert){
        button_8.click();
        button_sub.click();
        button_4.click();
        button_res.click();
        assert.equal($('print').val(),2,"8/4 must be 2");
        button_clear.click();
      });

      QUnit.test("Multiply Test", function(assert){
        button_8.click();
        button_sub.click();
        button_4.click();
        button_res.click();
        assert.equal($('print').val(),32,"8 * 4 must be 2");
        button_clear.click();
      });

      var button_1 = $('button:contains("1")');
      var button_sin = $('button:contains("sin")');
      var button_tan= $('button:contains("tan")');
      var button_log = $ ('button:contains("log")');

      QUnit.test("Sin Test", function(assert){}
      button_1.click();
      button_sin.click();
      assert.equal( $('#print').val(), 0.8415, "sin 1 must be 0.8415" );
      button_clear.click();
      });

      QUnit.test( "Tan Test", function( assert ) {
      button_1.click();
      button_tan.click();
      assert.equal( $('#print').val(), 1.5574077246549023, "tan 1 must be 1.5574077246549023" );
      button_clear.click();
      });

      QUnit.test( "Log Test", function( assert ) {
      button_1.click();
      button_log.click();
      assert.equal( $('#print').val(), 0, "Log 1 must be 0" );
      button_clear.click();
      });
});
