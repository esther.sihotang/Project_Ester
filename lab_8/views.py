from django.shortcuts import render
from django.http import HttpResponseRedirect
# Create your views here.

response = {}
def index(request):
    response['author'] = "Ester"
    html = 'lab_8/lab_8.html'
    return render(request, html, response)
